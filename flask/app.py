from flask import Flask, request, jsonify
from flask_cors import CORS
import psycopg2

app = Flask(__name__)
CORS(app)


def connect_psql():
    try:
        conn = psycopg2.connect("dbname=github_user_repos user=super password=super")
        cur = conn.cursor()
        return (cur, conn)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def close_psql_connection(cur):
    cur.close()

def format_username(unformatted):
    formatted = []
    for i in unformatted:
        formatted.append(i[0])
    return {"response": formatted}
    

@app.route('/usernames', methods=["GET"])
def getUsernames():
    cur, conn = connect_psql()
    cur.execute("SELECT DISTINCT author FROM repositories")
    resp = cur.fetchall()
    return format_username(resp)


@app.route('/repos', methods=["GET"])
def getRepos():
    args = request.args
    username = args.get('username')
    cur, conn = connect_psql()
    cur.execute("SELECT name FROM repositories WHERE author=%s", (username,))
    resp = cur.fetchall()
    print("SHANA", resp)
    return format_username(resp)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)     