# GItLabUsernames

## Getting started
The `scraper.py` file can be run as follows:
`python scraper --usernames name1 name2`
This script connects to a local psql database, so if you run it on you own machine it won't work unless you have the same database setup.


The Angular and Flask apps are run locally too. To start angular app, run `ng-serve` in github_project directory and `flask run` in flask directory


Once the scraper is run, the new usernames will appear on the app, after reloading. When choosing a username, it will who you all of that user's repos based on what is currently in the database
