import requests
from bs4 import BeautifulSoup
import argparse
import psycopg2
import uuid

def scrape_profile(username):
    url = f'https://github.com/{username}?tab=repositories'
    r=requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    repos = soup.find_all("a", itemprop="name codeRepository")#itemprop="name codeRepository"
    
    public_repositories = []
    for element in repos:
        public_repositories.append(element.text)
    return public_repositories


def connect_psql():
    try:
        conn = psycopg2.connect("dbname=github_user_repos user=super password=super")
        cur = conn.cursor()
        return (cur, conn)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def close_psql_connection(cur):
    cur.close()

def write_to_psql(cur, repos, author, conn):#with more time I would check that there are no duplicates
    for repo in repos:
        id = uuid.uuid1()
        cur.execute("INSERT INTO repositories (ID, NAME, AUTHOR) VALUES(%s, %s, %s)", (str(id), repo, author))
    conn.commit()

def main():
    parser = argparse.ArgumentParser(description="Scrape GitHub username")
    parser.add_argument('--usernames', action='store', type=str, nargs='+')

    args = parser.parse_args()
    usernames = args.usernames

    for username in usernames:
        repos = scrape_profile(username)
        cur, conn = connect_psql()
        write_to_psql(cur, repos, username, conn)
        # cur.execute('SELECT * FROM repositories')
        # test = cur.fetchall()
        # print(test)

    close_psql_connection(cur)


main()
#connect_psql()



