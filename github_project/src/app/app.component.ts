import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { response } from 'express';
import { GitHub } from './github.service';
import {MatButtonModule} from '@angular/material/button';
import { NgModel } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

//ulist:[]
export class AppComponent implements OnInit{
  ulist=[]
  selectedUser: string = "";
  title = 'github_project';
  showBotton: Boolean = false;
  repositories: string[] = []

  constructor(private git: GitHub){
  }

  ngOnInit(): void {
    this.getUsernames()
  }
  getUsernames(){
    this.git.getUsernames().subscribe(response_data =>{
      let usernames:any = response_data;
      console.log(usernames)
      this.ulist = usernames.response
    })
  }
  selected(){
    return this.selectedUser !='';
  }
  onChange(event:any){
    this.git.getRepositories(this.selectedUser).subscribe(response_data =>{
      let repos:any = response_data
      this.repositories = repos.response
    })
  }

}
