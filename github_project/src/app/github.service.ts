import { HttpClient } from '@angular/common/http';
import { Injectable} from '@angular/core';
import { rootCertificates } from 'tls';
import { HttpHeaders, HttpParams } from '@angular/common/http';

const headers= new HttpHeaders()
.set('content-type', 'application/json')
.set('Access-Control-Allow-Origin', '*');  

@Injectable({
    providedIn: 'root'
})
export class GitHub {
  
    constructor(private _http: HttpClient){}
    
    getUsernames() {
        let url = 'http://10.0.0.105:105/usernames';
        return this._http.get(url,{ 'headers': headers })
    }

    getRepositories(username:any){
        let queryParams = new HttpParams().append("username",username);
        let url = 'http://10.0.0.105:105/repos'
        
        return this._http.get(url,{params:queryParams})
    }
}
